Highcharts.chart('container1', {
    data: {
        table: 'datatable1'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});



Highcharts.chart('container2', {
    data: {
        table: 'datatable2'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});


Highcharts.chart('container3', {
    data: {
        table: 'datatable3'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});


Highcharts.chart('container4', {
    data: {
        table: 'datatable4'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container5', {
    data: {
        table: 'datatable5'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container6', {
    data: {
        table: 'datatable6'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container7', {
    data: {
        table: 'datatable7'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container8', {
    data: {
        table: 'datatable8'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container9', {
    data: {
        table: 'datatable9'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container10', {
    data: {
        table: 'datatable10'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});


Highcharts.chart('container11', {
    data: {
        table: 'datatable11'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});



Highcharts.chart('container12', {
    data: {
        table: 'datatable12'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});


Highcharts.chart('container13', {
    data: {
        table: 'datatable13'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});


Highcharts.chart('container14', {
    data: {
        table: 'datatable14'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container15', {
    data: {
        table: 'datatable15'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container16', {
    data: {
        table: 'datatable16'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container17', {
    data: {
        table: 'datatable17'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container18', {
    data: {
        table: 'datatable18'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container19', {
    data: {
        table: 'datatable19'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container20', {
    data: {
        table: 'datatable20'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});


Highcharts.chart('container21', {
    data: {
        table: 'datatable21'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});



Highcharts.chart('container22', {
    data: {
        table: 'datatable22'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});


Highcharts.chart('container23', {
    data: {
        table: 'datatable23'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});


Highcharts.chart('container24', {
    data: {
        table: 'datatable24'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container25', {
    data: {
        table: 'datatable25'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container26', {
    data: {
        table: 'datatable26'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container27', {
    data: {
        table: 'datatable27'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container28', {
    data: {
        table: 'datatable28'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container29', {
    data: {
        table: 'datatable29'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container30', {
    data: {
        table: 'datatable30'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});


Highcharts.chart('container31', {
    data: {
        table: 'datatable31'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});



Highcharts.chart('container32', {
    data: {
        table: 'datatable32'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});


Highcharts.chart('container33', {
    data: {
        table: 'datatable33'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});


Highcharts.chart('container34', {
    data: {
        table: 'datatable34'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container35', {
    data: {
        table: 'datatable35'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container36', {
    data: {
        table: 'datatable36'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container37', {
    data: {
        table: 'datatable37'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container38', {
    data: {
        table: 'datatable38'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container39', {
    data: {
        table: 'datatable39'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container40', {
    data: {
        table: 'datatable40'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});


Highcharts.chart('container41', {
    data: {
        table: 'datatable41'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});



Highcharts.chart('container42', {
    data: {
        table: 'datatable42'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});


Highcharts.chart('container43', {
    data: {
        table: 'datatable43'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});


Highcharts.chart('container44', {
    data: {
        table: 'datatable44'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container45', {
    data: {
        table: 'datatable45'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container46', {
    data: {
        table: 'datatable46'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container47', {
    data: {
        table: 'datatable47'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container48', {
    data: {
        table: 'datatable48'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container49', {
    data: {
        table: 'datatable49'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container50', {
    data: {
        table: 'datatable50'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});





Highcharts.chart('container51', {
    data: {
        table: 'datatable51'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});



Highcharts.chart('container52', {
    data: {
        table: 'datatable52'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});


Highcharts.chart('container53', {
    data: {
        table: 'datatable53'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});


Highcharts.chart('container54', {
    data: {
        table: 'datatable54'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container55', {
    data: {
        table: 'datatable55'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container56', {
    data: {
        table: 'datatable56'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container57', {
    data: {
        table: 'datatable57'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container58', {
    data: {
        table: 'datatable58'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container59', {
    data: {
        table: 'datatable59'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container60', {
    data: {
        table: 'datatable60'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});


Highcharts.chart('container61', {
    data: {
        table: 'datatable61'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});



Highcharts.chart('container62', {
    data: {
        table: 'datatable62'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});


Highcharts.chart('container63', {
    data: {
        table: 'datatable63'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});


Highcharts.chart('container64', {
    data: {
        table: 'datatable64'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container65', {
    data: {
        table: 'datatable65'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container66', {
    data: {
        table: 'datatable66'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container67', {
    data: {
        table: 'datatable67'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container68', {
    data: {
        table: 'datatable68'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container69', {
    data: {
        table: 'datatable69'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container70', {
    data: {
        table: 'datatable70'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});





Highcharts.chart('container71', {
    data: {
        table: 'datatable71'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});



Highcharts.chart('container72', {
    data: {
        table: 'datatable72'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});


Highcharts.chart('container73', {
    data: {
        table: 'datatable73'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});


Highcharts.chart('container74', {
    data: {
        table: 'datatable74'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container75', {
    data: {
        table: 'datatable75'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container76', {
    data: {
        table: 'datatable76'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container77', {
    data: {
        table: 'datatable77'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container78', {
    data: {
        table: 'datatable78'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container79', {
    data: {
        table: 'datatable79'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container80', {
    data: {
        table: 'datatable80'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container81', {
    data: {
        table: 'datatable81'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});



Highcharts.chart('container82', {
    data: {
        table: 'datatable82'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});


Highcharts.chart('container83', {
    data: {
        table: 'datatable83'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});


Highcharts.chart('container84', {
    data: {
        table: 'datatable84'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container85', {
    data: {
        table: 'datatable85'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container86', {
    data: {
        table: 'datatable86'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container87', {
    data: {
        table: 'datatable87'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container88', {
    data: {
        table: 'datatable88'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

Highcharts.chart('container89', {
    data: {
        table: 'datatable89'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: '선수별 예측 연봉'
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: '연봉'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    }
});

